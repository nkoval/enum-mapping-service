package com.acx.mapping.auth;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.inject.Inject;
import javax.inject.Singleton;

import com.acx.mapping.utils.IO;
import com.typesafe.config.Config;

import play.Logger;
import play.libs.ws.WSRequest;

@Singleton
public class Authentication
{
    private final static Logger.ALogger logger = play.Logger.of(Authentication.class);
    private static final String cookieFilePath = "authentication.cookieFile";
    private final String cookies;

    @Inject
    public Authentication(Config config) throws IOException, URISyntaxException
    {
        if (config.hasPath(cookieFilePath))
        {
            String file = config.getString(cookieFilePath);
            if (file.isEmpty())
            {
                throw new RuntimeException(String.format("No %s configured", cookieFilePath));
            }
            else
            {
                logger.debug("Reading cookies from: {}", file);
                this.cookies = readFile(file);
                logger.debug("Cookies read: {}", this.cookies);
            }
        }
        else
        {
            throw new RuntimeException(String.format("No %s configured", cookieFilePath));
        }
    }

    private String readFile(final String fileName) throws IOException, URISyntaxException
    {

        final URL url = getClass().getClassLoader().getResource(fileName);

        if (url == null)
            throw new RuntimeException(String.format("Could not find file: '%s'", fileName));

        final Path path = Paths.get(url.toURI());
        return IO.readFile(path);
    }

    public String getCookies()
    {
        return cookies;
    }

    public WSRequest addCookies(WSRequest request)
    {
        request.addHeader("Cookie", cookies);
        return request;
    }
}
