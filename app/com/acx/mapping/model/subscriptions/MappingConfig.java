package com.acx.mapping.model.subscriptions;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class MappingConfig
{
    private final String id;
    private final String name;
    private final String type;
    private final Map<String, String> config;

    @JsonCreator
    public MappingConfig(@JsonProperty("id") final String id, @JsonProperty("name") final String name,
        @JsonProperty("type") final String type, @JsonProperty("config") final Map<String, String> config)
    {
        this.id = id;
        this.name = name;
        this.type = type;
        this.config = config;
    }

    public String getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }

    public String getType()
    {
        return type;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        MappingConfig that = (MappingConfig) o;
        return id.equals(that.id) && name.equals(that.name) && type.equals(that.type) &&
            getConfig().equals(that.getConfig());
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(id, name, type, getConfig());
    }

    @Override
    public String toString()
    {
        return "{" + "id='" + id + '\'' + ", name='" + name + '\'' + ", type='" + type + '\'' + ", mapping=" +
            getConfig() + '}';
    }

    public Map<String, String> getConfig()
    {
        return config;
    }
}
