package com.acx.mapping.services.tracking;

public enum MessageLevel
{
    INFO, WARNING, ERROR
}
