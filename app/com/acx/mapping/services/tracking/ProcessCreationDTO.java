package com.acx.mapping.services.tracking;

public class ProcessCreationDTO
{
    private final String name;
    private final String processId;
    private final ProcessStatus status = ProcessStatus.PROGRESS;
    private final String stage;

    public ProcessCreationDTO(final String name, final String processId, final String stage)
    {
        this.name = name;
        this.processId = processId;
        this.stage = stage;
    }

    public String getName()
    {
        return name;
    }

    public String getProcessId()
    {
        return processId;
    }

    public ProcessStatus getStatus()
    {
        return status;
    }

    public String getStage()
    {
        return stage;
    }
}
