package com.acx.mapping.services.tracking;

public class ProcessUpdateDTO
{
    private final String message;
    private final MessageLevel messageLevel;
    private final ProcessStatus status;

    public ProcessUpdateDTO(final String message, final MessageLevel messageLevel, final ProcessStatus status)
    {
        this.message = message;
        this.messageLevel = messageLevel;
        this.status = status;
    }

    public String getMessage()
    {
        return message;
    }

    public MessageLevel getMessageLevel()
    {
        return messageLevel;
    }

    public ProcessStatus getStatus()
    {
        return status;
    }
}
