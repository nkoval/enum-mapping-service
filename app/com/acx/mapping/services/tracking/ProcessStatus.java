package com.acx.mapping.services.tracking;

public enum ProcessStatus
{
    PROGRESS, SUCCESSFUL, FAILED,
}
