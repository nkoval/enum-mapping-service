package com.acx.mapping.modules;

import java.util.List;

import com.acx.mapping.repository.MappingsRepository;
import com.acx.mapping.repository.cassandra.CassandraMappingsRepository;
import com.google.common.collect.Lists;
import com.typesafe.config.Config;

import play.Environment;
import play.Logger;
import play.inject.Binding;
import play.inject.Module;

/**
 * Module called at startup to bind configuration options
 */
public class RepositoryModule extends Module
{

    private final Logger.ALogger logger = Logger.of(this.getClass());

    @Override
    public List<Binding<?>> bindings(Environment environment, final Config config)
    {

        return Lists.newArrayList(
            bindClass(MappingsRepository.class).to(CassandraMappingsRepository.class));
    }
}
