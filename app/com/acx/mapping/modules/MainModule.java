package com.acx.mapping.modules;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.google.common.collect.Lists;
import com.google.inject.AbstractModule;
import com.typesafe.config.Config;

import play.Environment;
import play.Logger;
import play.inject.Binding;

import play.inject.Module;
import play.libs.Json;

/**
 * Module called at startup to bind configuration options
 */
public class MainModule extends Module
{

    private final Logger.ALogger logger = play.Logger.of(this.getClass());

    @Override
    public List<Binding<?>> bindings(Environment environment, final Config config)
    {

        return Lists.newArrayList(
            bindClass(ObjectMapper.class).qualifiedWith("validationMapper").toInstance(getValidationMapper(config)),
            bindClass(Integer.class).qualifiedWith("batchSize").toInstance(getInteger(config, "batchSize", 1000, 1)));
    }

    private ObjectMapper getValidationMapper(final Config config)
    {
        if (getBoolean(config, "validation.ignoreUnknownProperties", true))
        {
            return Json.mapper(); // default
        }
        else
        {
            ObjectMapper mapper = new ObjectMapper();
            mapper.registerModule(new Jdk8Module());
            mapper.registerModule(new JavaTimeModule());
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, true);
            return mapper;
        }
    }

    private boolean getBoolean(Config config, final String path, boolean defaultValue)
    {
        boolean value = defaultValue;
        try
        {
            if (config.hasPath(path))
            {
                value = config.getBoolean(path);
            }
        }
        catch (RuntimeException e)
        {
            logger.error("Error in configuration for {}: {}. Falling back to default: {}", path, e.getMessage(),
                defaultValue);
        }
        logger.debug("{}: {}", path, value);
        return value;
    }

    private int getInteger(Config config, final String path, int defaultValue, int minimumValue)
    {
        int value = defaultValue;
        try
        {
            if (config.hasPath(path))
            {
                value = config.getInt(path);
                if (value < minimumValue)
                {
                    throw new RuntimeException("value must not be less than " + minimumValue);
                }
            }
        }
        catch (RuntimeException e)
        {
            logger.error("Error in configuration for {}: {}. Falling back to default: {}", path, e.getMessage(),
                defaultValue);
            value = defaultValue;
        }
        logger.debug("{}: {}", path, value);
        return value;
    }
}
