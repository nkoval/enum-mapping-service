package com.acx.mapping.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class GeneralUtils
{

    /**
     * URI segment encoder, Java version of https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/encodeURIComponent
     * Different from URLEncoder, which is why encoding of certain symbols is reverted. Explanation in the above URL.
     *
     * @param s input URL segment
     * @return encoded URL segment
     */
    public static String encodeURIComponent(String s)
    {

        String result;

        try
        {
            result = URLEncoder.encode(s, "UTF-8").replaceAll("\\+", "%20").replaceAll("\\%21", "!").replaceAll("\\%27",
                "'").replaceAll("\\%28", "(").replaceAll("\\%29", ")").replaceAll("\\%7E", "~");
        }
        catch (UnsupportedEncodingException e)
        {
            result = s;
        }

        return result;

    }

}