package com.acx.mapping.utils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonUtils
{
    public static <A> A fromJson(ObjectMapper validationMapper, JsonNode json, Class<A> clazz)
    {
        try
        {
            return validationMapper.treeToValue(json, clazz);
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }
}
