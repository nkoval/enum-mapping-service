package com.acx.mapping.utils;

import javax.annotation.Nullable;
import java.util.Optional;

/**
 * This class represents an instance that can optionally have a paired object, e.g. the result of a TS query as an
 * addition to the static one
 *
 * @param <T>
 */
public class OptionalPair<T>
{
    final Optional<T> left;

    final Optional<T> right;

    public OptionalPair(Optional<T> left, Optional<T> right)
    {
        this.left = left;
        this.right = right;
    }

    public static <T> OptionalPair<T> of(T left, T right)
    {
        return new OptionalPair<>(Optional.ofNullable(left), Optional.ofNullable(right));
    }

    public T getLeft()
    {
        return left.get();
    }

    public boolean isPresent()
    {
        return left.isPresent() || right.isPresent();
    }

    public boolean isLeft()
    {
        return left.isPresent();
    }

    public boolean isRight()
    {
        return right.isPresent();
    }

    public T getRight()
    {
        return right.get();
    }
}
