package com.acx.mapping.utils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.stream.Stream;

import akka.http.javadsl.model.DateTime;

public class IO
{

    private static final String DOT = ".";

    public static String readFile(final Path path) throws IOException
    {
        StringBuilder sb = new StringBuilder();

        try (Stream<String> stream = Files.lines(path))
        {
            stream.forEach(line -> sb.append(line));
        }

        return sb.toString();
    }

    private static String sanitizeFilename(String original)
    {
        return original.replace(":", "").replace(" ", "_");
    }

    /**
     * Generates and sanitizes filename
     *
     * @param nameAndExtension file name pieces that will be concatenated using {@link #DOT}. There should be at least
     *                         two segments, and the last is the filename extension (without the "." sign)
     * @return the generated and sanitized filename as a String
     */
    public static String generateFileName(String... nameAndExtension)
    {
        if (nameAndExtension.length < 2)
            throw new IllegalArgumentException(
                "Filename should consist of at least two parts: name and extension. Was: " +
                    Arrays.toString(nameAndExtension));

        final int paramsLength = nameAndExtension.length - 1;
        StringBuilder withoutExtension =
            new StringBuilder(String.join(DOT, Arrays.copyOf(nameAndExtension, paramsLength)));

        withoutExtension.append(DOT).append(DateTime.now().toIsoDateTimeString());

        return sanitizeFilename(withoutExtension.append(DOT).append(nameAndExtension[paramsLength]).toString());
    }

}
