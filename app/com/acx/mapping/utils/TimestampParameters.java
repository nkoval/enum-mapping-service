package com.acx.mapping.utils;

import java.time.Instant;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.Optional;
import java.util.TimeZone;

import org.joda.time.DateTime;

class TimestampParameters
{

    final boolean fromToRecordedOn;
    long from;
    long to;

    String error;

    TimestampParameters()
    {
        this(true);
    }

    TimestampParameters(boolean fromToRecordedOn)
    {
        this.fromToRecordedOn = fromToRecordedOn;
    }

    private static long toEpochMilli(final String value)
    {
        try
        {
            return Long.parseLong(value);
        }
        catch (Exception e)
        {
            return DateTime.parse(value).getMillis();
        }
    }

    private static long getTime(DefaultStrategy strategy)
    {
        switch (strategy)
        {
            case CURRENT_TIME:
                return Instant.now().toEpochMilli();
            case MINIMUM_TIME:
                return Long.MIN_VALUE;
            case LAST_24_HOURS:
                Calendar cal = new GregorianCalendar(TimeZone.getTimeZone("GMT"), Locale.UK);
                cal.add(Calendar.DATE, -1);
                return cal.getTimeInMillis();
            default:
                throw new UnsupportedOperationException("unknown default strategy");
        }
    }

    boolean parseFrom(String value)
    {
        try
        {
            from = toEpochMilli(value);
            return true;
        }
        catch (UnsupportedOperationException | IllegalArgumentException e)
        {
            error = String.format("Failed parsing '%s' (from): %s", value, e.getMessage());
            return false;
        }
    }

    boolean parseTo(String value)
    {
        try
        {
            to = toEpochMilli(value);
            return true;
        }
        catch (UnsupportedOperationException | IllegalArgumentException e)
        {
            error = String.format("Failed parsing '%s' (to): %s", value, e.getMessage());
            return false;
        }
    }

    boolean parseFrom(Optional<String> value, DefaultStrategy defaultStrategy)
    {
        if (value.isPresent())
            return parseFrom(value.get());
        else
        {
            from = getTime(defaultStrategy);
            return true;
        }
    }

    boolean parseTo(Optional<String> value, DefaultStrategy defaultStrategy)
    {
        if (value.isPresent())
            return parseTo(value.get());
        else
        {
            to = getTime(defaultStrategy);
            return true;
        }
    }

    boolean finish()
    {
        if (from > to)
        {
            error = String.format("Invalid time range specified: '%s' (from) is greater than '%s' (to)",
                timestampToIsoString(from), timestampToIsoString(to));
            return false;
        }
        return true;
    }

    private String timestampToIsoString(long timestamp)
    {
        return Instant.ofEpochMilli(timestamp).toString();
    }

    enum DefaultStrategy
    {
        CURRENT_TIME, MINIMUM_TIME, LAST_24_HOURS
    }
}

