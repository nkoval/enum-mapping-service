package com.acx.mapping.exceptions;

public class InitializingException extends RuntimeException
{
    public InitializingException(String msg)
    {
        super(msg);
    }

    public InitializingException(Throwable t)
    {
        super(t);
    }
}
