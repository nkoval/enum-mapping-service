package com.acx.mapping.controllers;

import static play.libs.Json.fromJson;
import static play.libs.Json.toJson;

import java.util.Optional;

import javax.inject.Inject;

import com.acx.mapping.model.subscriptions.MappingConfig;
import com.acx.mapping.repository.MappingsRepository;
import com.fasterxml.jackson.databind.JsonNode;

import play.Logger;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;

public class MappingsController extends Controller
{
    private final static Logger.ALogger logger = Logger.of(MappingsController.class);
    private final MappingsRepository repository;

    @Inject
    public MappingsController(final MappingsRepository repository)
    {
        this.repository = repository;
    }

    private void logRequest(final Http.Request request)
    {
        logger.debug("A {} request was received at {}", request.method(), request.uri());
    }

    public Result getAll(final Http.Request request)
    {
        logRequest(request);

        return ok(toJson(repository.readAll()));
    }

    public Result create(final Http.Request request)
    {
        logger.debug("Creating Transformation: {} ", request.body().asJson());
        MappingConfig transformation = fromJson(request.body().asJson(), MappingConfig.class);

        // TODO: validate
        MappingConfig createdTr = repository.create(transformation);
        JsonNode content = toJson(createdTr);
        Result result = created(content).withHeader(LOCATION, createdTr.getId());
        return result;
    }

    public Result update(final String id, final Http.Request request)
    {
        logger.debug("Updating Transformation: {}", request.body().asJson());
        MappingConfig newTransformation = fromJson(request.body().asJson(), MappingConfig.class);
        // String error = validate(transformation);
        // if(error != null){
        // return badRequest(error);
        // }
        Optional<MappingConfig> oldTransformation = repository.read(id);
        if (oldTransformation.isPresent())
        {
            repository.update(id, newTransformation);
            // String username = getRequestUser(request);
            // changeLog.logTransformationUpdated(oldTransformation.get(), transformation, username);
            return ok();
        }
        else
        {
            return notFound(String.format("Transformation with ID '%s' not found", id));
        }
    }

    public Result delete(final String id, final Http.Request request)
    {
        logger.debug("Deleting Transformation {} ", id);
        repository.delete(id);
        return ok();
    }
}
