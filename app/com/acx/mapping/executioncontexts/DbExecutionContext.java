package com.acx.mapping.executioncontexts;

import javax.inject.Inject;

import akka.actor.ActorSystem;
import play.libs.concurrent.CustomExecutionContext;

/**
 * Configurable execution context for database read operations
 */
public class DbExecutionContext extends CustomExecutionContext
{

    @Inject
    public DbExecutionContext(ActorSystem actorSystem)
    {
        super(actorSystem, "contexts.dbOperations");
    }
}