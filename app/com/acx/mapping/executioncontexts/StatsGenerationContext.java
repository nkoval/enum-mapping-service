package com.acx.mapping.executioncontexts;

import javax.inject.Inject;

import akka.actor.ActorSystem;
import play.libs.concurrent.CustomExecutionContext;

/**
 * Configurable execution context for task scheduling operations
 *
 * @author ldejong
 */
public class StatsGenerationContext extends CustomExecutionContext
{

    @Inject
    public StatsGenerationContext(ActorSystem actorSystem)
    {
        super(actorSystem, "contexts.statsGenerator");
    }
}