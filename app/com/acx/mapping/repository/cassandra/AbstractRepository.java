package com.acx.mapping.repository.cassandra;

import com.acx.mapping.repository.PersistenceException;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Statement;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

abstract class AbstractRepository
{
    final CassandraCluster cluster;
    private final ObjectMapper mapper;

    AbstractRepository(final CassandraCluster cluster, final ObjectMapper mapper)
    {
        this.cluster = cluster;
        this.mapper = mapper;
    }

    ResultSet execute(final Statement statement)
    {
        return cluster.getSession().execute(statement);
    }

    protected String toJson(final Object object)
    {
        try
        {
            return mapper.writeValueAsString(object);
        }
        catch (JsonProcessingException e)
        {
            throw new PersistenceException("Could not serialize data model to JSON", e);
        }
    }

    protected <T> T fromJson(final String json, final Class<T> clazz)
    {
        try
        {
            return mapper.readValue(json, clazz);
        }
        catch (JsonProcessingException e)
        {
            throw new PersistenceException("Could parse JSON string", e);
        }
    }

    protected <T> T fromJson(final String json, final TypeReference<T> valueTypeRef)
    {
        try
        {
            return mapper.readValue(json, valueTypeRef);
        }
        catch (JsonProcessingException e)
        {
            throw new PersistenceException("Could parse JSON string", e);
        }
    }

}
