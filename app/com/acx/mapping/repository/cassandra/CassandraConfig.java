package com.acx.mapping.repository.cassandra;

import com.google.common.base.MoreObjects;
import com.typesafe.config.Config;

import play.Logger;

import java.util.List;
import java.util.Optional;

public class CassandraConfig
{

    private static final String CONFIG_CONTACT_POINTS = "contactPoints";
    private static final String CONFIG_PORT = "port";
    private static final String CONFIG_RECONNECTION_POLICY = "reconnectionPolicy";
    private static final String CONFIG_QUERY_LOGGER = "queryLogger";
    private static final String CONFIG_SCHEMA_FILE = "schemaFile";
    private static final String CONFIG_CREATE_SCHEMA = "createSchemaOnStartup";
    private static final String CONFIG_KEYSPACE = "keyspace";
    private static final String DEFAULT_KEYSPACE = "batch_publisher";
    private final Logger.ALogger logger = play.Logger.of(this.getClass());
    private final List<String> contactPoints;
    private final Integer port;
    private final String schemaFile;
    private final boolean createSchemaOnStartup;
    private final String keyspace;

    private final CassandraReconnectionConfig reconnectionConfig;
    private final CassandraQueryLoggerConfig queryLoggerConfig;

    public CassandraConfig(Config config)
    {
        this.contactPoints = config.getStringList(CONFIG_CONTACT_POINTS);

        this.port = config.hasPath(CONFIG_PORT) ? config.getInt(CONFIG_PORT) : null;

        this.reconnectionConfig = config.hasPath(CONFIG_RECONNECTION_POLICY) ?
                                  new CassandraReconnectionConfig(config.getConfig(CONFIG_RECONNECTION_POLICY)) : null;

        this.queryLoggerConfig = config.hasPath(CONFIG_QUERY_LOGGER) ?
                                 new CassandraQueryLoggerConfig(config.getConfig(CONFIG_QUERY_LOGGER)) : null;

        this.createSchemaOnStartup = config.hasPath(CONFIG_CREATE_SCHEMA) && config.getBoolean(CONFIG_CREATE_SCHEMA);

        this.schemaFile = config.hasPath(CONFIG_SCHEMA_FILE) ? config.getString(CONFIG_SCHEMA_FILE) : null;

        this.keyspace = config.hasPath(CONFIG_KEYSPACE) ? config.getString(CONFIG_KEYSPACE) : DEFAULT_KEYSPACE;

        if (createSchemaOnStartup && schemaFile == null)
        {
            logger.warn("'{}' set to true but '{}' is not set", CONFIG_CREATE_SCHEMA, CONFIG_SCHEMA_FILE);
            throw new RuntimeException(
                String.format("'%s' set to true but '%s' is not set", CONFIG_CREATE_SCHEMA, CONFIG_SCHEMA_FILE));
        }
    }

    public List<String> getContactPoints()
    {
        return contactPoints;
    }

    public Optional<Integer> getPort()
    {
        return Optional.ofNullable(port);
    }

    public Optional<CassandraReconnectionConfig> getReconnectionConfig()
    {
        return Optional.ofNullable(reconnectionConfig);
    }

    public Optional<CassandraQueryLoggerConfig> getQueryLoggerConfig()
    {
        return Optional.ofNullable(queryLoggerConfig);
    }

    @Override
    public String toString()
    {
        return MoreObjects.toStringHelper(this).add("contactPoints", contactPoints).add("port", port).add(
            "reconnectionConfig", reconnectionConfig).add("queryLoggerConfig", queryLoggerConfig).add("keyspace",
            keyspace).toString();
    }

    public boolean createSchemAtStartup()
    {
        return createSchemaOnStartup;
    }

    public Optional<String> getSchemaFile()
    {
        return Optional.ofNullable(schemaFile);
    }

    public String getKeyspace()
    {
        return keyspace;
    }
}
