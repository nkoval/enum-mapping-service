package com.acx.mapping.repository.cassandra;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.QueryLogger;
import com.google.common.base.MoreObjects;
import com.typesafe.config.Config;

import play.Logger.ALogger;

import java.util.Optional;

public class CassandraQueryLoggerConfig
{

    private static final String PROPS_ENABLED = "enabled";
    private static final String PROPS_SLOW_QUERY_LATENCY_THRESHOLD_MILLIS = "slowQueryLatencyThresholdMillis";
    private static final String PROPS_MAX_QUERY_STRING_LENGTH = "maxQueryStringLength";
    private static final String PROPS_MAX_LOGGED_PARAMETERS = "maxLoggedParameters";
    private static final String PROPS_MAX_PARAMETER_VALUE_LENGTH = "maxParameterValueLength";

    private boolean enabled = false;
    private Long constantThreshold = QueryLogger.DEFAULT_SLOW_QUERY_THRESHOLD_MS;
    private Integer maxQueryStringLength = QueryLogger.DEFAULT_MAX_QUERY_STRING_LENGTH;
    private Integer maxLoggedParameters = QueryLogger.DEFAULT_MAX_LOGGED_PARAMETERS;
    private Integer maxParameterValueLength = QueryLogger.DEFAULT_MAX_PARAMETER_VALUE_LENGTH;

    public CassandraQueryLoggerConfig(Config config)
    {
        if (config.hasPath(PROPS_ENABLED) && config.getBoolean(PROPS_ENABLED))
        {
            // enabled
            this.enabled = true;

            if (config.hasPath(PROPS_SLOW_QUERY_LATENCY_THRESHOLD_MILLIS))
            {
                this.constantThreshold = config.getLong(PROPS_SLOW_QUERY_LATENCY_THRESHOLD_MILLIS);
            }

            if (config.hasPath(PROPS_MAX_QUERY_STRING_LENGTH))
            {
                maxQueryStringLength = config.getInt(PROPS_MAX_QUERY_STRING_LENGTH);
            }

            if (config.hasPath(PROPS_MAX_LOGGED_PARAMETERS))
            {
                maxLoggedParameters = config.getInt(PROPS_MAX_LOGGED_PARAMETERS);
            }

            if (config.hasPath(PROPS_MAX_PARAMETER_VALUE_LENGTH))
            {
                maxParameterValueLength = config.getInt(PROPS_MAX_PARAMETER_VALUE_LENGTH);
            }
        }
    }

    public Optional<QueryLogger> createLoggerConfig(Cluster.Builder builder, Optional<ALogger> logger)
    {
        Optional<QueryLogger> out = Optional.empty();
        if (enabled)
        {
            logger.ifPresent(l -> l.info("Using custom query logger with parameters: {}", this.toString()));

            QueryLogger.Builder queryLoggerBuilder = QueryLogger.builder();
            queryLoggerBuilder.withConstantThreshold(constantThreshold);

            QueryLogger queryLogger = queryLoggerBuilder.build();
            queryLogger.setMaxQueryStringLength(maxQueryStringLength);
            queryLogger.setMaxLoggedParameters(maxLoggedParameters);
            queryLogger.setMaxParameterValueLength(maxParameterValueLength);

            // assign value
            out = Optional.of(queryLogger);
        }
        else
        {
            logger.ifPresent(l -> l.info("Using standard query logger"));
        }
        return out;
    }

    @Override
    public String toString()
    {
        return MoreObjects.toStringHelper(this).add("enabled", enabled).add("constantThreshold", constantThreshold).add(
            "maxQueryStringLength", maxQueryStringLength).add("maxLoggedParameters", maxLoggedParameters).add(
            "maxParameterValueLength", maxParameterValueLength).toString();
    }
}
