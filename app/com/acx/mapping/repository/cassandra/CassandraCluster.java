package com.acx.mapping.repository.cassandra;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

import javax.inject.Inject;
import javax.inject.Singleton;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.exceptions.NoHostAvailableException;
import com.datastax.driver.core.policies.ReconnectionPolicy;
import com.datastax.driver.core.policies.ReconnectionPolicy.ReconnectionSchedule;
import com.typesafe.config.Config;

import play.Logger;
import play.Logger.ALogger;
import play.inject.ApplicationLifecycle;

@Singleton
public class CassandraCluster
{

    private final Logger.ALogger logger = play.Logger.of(this.getClass());

    private final String keyspace;
    private Cluster cluster;
    private Session session;
    private boolean running = true;

    @Inject
    public CassandraCluster(ApplicationLifecycle lifecycle, Config appConfig)
    {
        final CassandraConfig config = new CassandraConfig(appConfig.getConfig("cassandra"));
        keyspace = config.getKeyspace();

        Cluster.Builder clusterBuilder = createClusterBuilder(config);
        logger.info("Applying cassandra configuration {}", config);
        lifecycle.addStopHook(this::onStop);
        cluster = createCluster(clusterBuilder, config, logger);

        createSession(clusterBuilder, config);
    }

    private Cluster.Builder createClusterBuilder(CassandraConfig config)
    {
        Cluster.Builder builder = Cluster.builder();
        config.getContactPoints().forEach(builder::addContactPoint);
        config.getPort().ifPresent(builder::withPort);
        config.getReconnectionConfig().ifPresent(p -> p.applyReconnectionPolicy(builder));
        return builder;
    }

    private Cluster createCluster(Cluster.Builder clusterBuilder, CassandraConfig config, ALogger logger)
    {
        Cluster cluster = clusterBuilder.build();
        config.getQueryLoggerConfig().flatMap(
            l -> l.createLoggerConfig(clusterBuilder, Optional.ofNullable(logger))).ifPresent(cluster::register);
        return cluster;
    }

    /**
     * Tries to setup initial connection to Cassandra using the configured retry policy.
     *
     * @return the session
     */
    private void createSession(Cluster.Builder clusterBuilder, CassandraConfig config)
    {

        Optional<CassandraReconnectionConfig> reconnectionConfig = config.getReconnectionConfig();
        ReconnectionPolicy reconnectionPolicy;
        if (reconnectionConfig.isPresent())
            reconnectionPolicy = reconnectionConfig.get().getReconnectionPolicy(null);
        else
            reconnectionPolicy = cluster.getConfiguration().getPolicies().getReconnectionPolicy();

        ReconnectionSchedule schedule = reconnectionPolicy.newSchedule();

        do
        {
            try
            {
                this.session = cluster.connect();
                logger.info("Connected to Cassandra cluster: {}", cluster.getMetadata().getClusterName());
                break;
            }
            catch (NoHostAvailableException e)
            {
                long delayMs = schedule.nextDelayMs();
                logger.warn("Cannot connect to Cassandra, scheduling retry in {} milliseconds: {}", delayMs,
                    e.getMessage());
                // recreate cluster as it might have been closed
                cluster = createCluster(clusterBuilder, config, null);
                try
                {
                    Thread.sleep(delayMs);
                }
                catch (InterruptedException ex)
                {
                    throw new RuntimeException("Thread interrupted", ex);
                }
            }
            logger.debug("running: {}", running);
        }
        while (running);

        if (config.createSchemAtStartup())
        {
            config.getSchemaFile().ifPresent(f -> createSchema(session, f));
        }
    }

    public Session getSession()
    {
        return session;
    }

    public Cluster getCluster()
    {
        return cluster;
    }

    public String getKeyspace()
    {
        return keyspace;
    }

    public String getTable(String table)
    {
        return keyspace + "." + table;
    }

    private CompletionStage<Void> onStop()
    {
        return CompletableFuture.runAsync(() -> {
            logger.info("Stopping cassandra cluster");
            running = false;
            cluster.close();
        });
    }

    ;

    private void createSchema(Session session, final String schemaFile)
    {
        try
        {
            File file = new File(schemaFile);
            if (file.exists())
            {
                logger.info("Creating schema using file '{}'", schemaFile);
                final String content = new String(Files.readAllBytes(Paths.get(schemaFile)));

                final String[] cqlSentences = content.split(";");
                for (String cqlSentence : cqlSentences)
                {
                    if (!cqlSentence.trim().isEmpty())
                    {
                        session.execute(cqlSentence);
                    }
                }
                logger.info("Schema created");
            }
            else
            {
                logger.error("Schema not created: file '" + schemaFile + "' not found");
            }
        }
        catch (Exception ex)
        {
            logger.error("Creating schema failed with exception: {}", ex.getMessage());
        }
    }
}
