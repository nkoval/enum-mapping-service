package com.acx.mapping.repository.cassandra;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.policies.ConstantReconnectionPolicy;
import com.datastax.driver.core.policies.ExponentialReconnectionPolicy;
import com.datastax.driver.core.policies.ReconnectionPolicy;
import com.google.common.base.MoreObjects;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import play.Logger;
import play.Logger.ALogger;

public class CassandraReconnectionConfig
{
    private static final String PROPS_BASE_DELAY_MS = "baseDelayMs";
    private static final String PROPS_TYPE = "type";
    private static final String PROPS_MAX_DELAY_MS = "maxDelayMs";
    private static final String PROPS_DELAY_MS = "delayMs";
    private final Logger.ALogger logger = play.Logger.of(this.getClass());
    private ReconnectionConfig reconnectionConfig;

    public CassandraReconnectionConfig(Config config)
    {

        if (config.hasPath(PROPS_TYPE))
        {
            PolicyType policyType = PolicyType.valueOf(config.getString(PROPS_TYPE).toUpperCase());

            switch (policyType)
            {
                case EXPONENTIAL:
                    long baseDelayMs = 1000; // default: 1 second
                    if (config.hasPath(PROPS_BASE_DELAY_MS))
                    {
                        baseDelayMs = config.getInt(PROPS_BASE_DELAY_MS);
                    }
                    long maxDelayMs = 600_000; // default: 10 minutes
                    if (config.hasPath(PROPS_MAX_DELAY_MS))
                    {
                        maxDelayMs = config.getInt(PROPS_MAX_DELAY_MS);
                    }
                    reconnectionConfig = new ExponentialReconnection(baseDelayMs, maxDelayMs);
                    break;
                case CONSTANT:
                    int delay = config.getInt(PROPS_DELAY_MS);
                    reconnectionConfig = new ConstantReconnection(delay);
                    break;
                default:
                    throw new RuntimeException("Invalid policy type specified");
            }
        }
        else
            throw new RuntimeException("Policy type not specified");
    }

    public static CassandraReconnectionConfig createConstantConfig(long delayMs)
    {
        return new CassandraReconnectionConfig(
            ConfigFactory.parseString(PROPS_TYPE + "='constant'," + PROPS_DELAY_MS + "=" + delayMs));
    }

    /**
     * Apply reconnection policy to cassandra builder
     *
     * @param builder
     */
    public void applyReconnectionPolicy(Cluster.Builder builder)
    {
        builder.withReconnectionPolicy(reconnectionConfig.getReconnectionPolicy(logger));
    }

    public ReconnectionPolicy getReconnectionPolicy(ALogger logger)
    {
        return reconnectionConfig.getReconnectionPolicy(logger);
    }

    @Override
    public String toString()
    {
        return reconnectionConfig.toString();
    }

    private enum PolicyType
    {
        EXPONENTIAL, CONSTANT
    }

    private static interface ReconnectionConfig
    {
        ReconnectionPolicy getReconnectionPolicy(ALogger logger);
    }

    private static class ExponentialReconnection implements ReconnectionConfig
    {
        private final long baseDelayMs;
        private final long maxDelayMs;

        public ExponentialReconnection(long baseDelayMs, long maxDelayMs)
        {
            this.baseDelayMs = baseDelayMs;
            this.maxDelayMs = maxDelayMs;
        }

        @Override
        public String toString()
        {
            return MoreObjects.toStringHelper(this).add("baseDelayMs", baseDelayMs).add("maxDelayMs",
                maxDelayMs).toString();
        }

        @Override
        public ReconnectionPolicy getReconnectionPolicy(ALogger logger)
        {
            if (logger != null)
                logger.info("Using exponential reconnection policy: {}", this);
            return new ExponentialReconnectionPolicy(baseDelayMs, maxDelayMs);
        }
    }

    private static class ConstantReconnection implements ReconnectionConfig
    {
        private final long delayMs;

        private ConstantReconnection(long delayMs)
        {
            this.delayMs = delayMs;
        }

        @Override
        public String toString()
        {
            return MoreObjects.toStringHelper(this).add("delayMs", delayMs).toString();
        }

        @Override
        public ReconnectionPolicy getReconnectionPolicy(ALogger logger)
        {
            if (logger != null)
                logger.info("Using constant reconnection policy: {}", this);
            return new ConstantReconnectionPolicy(delayMs);
        }
    }
}
