package com.acx.mapping.repository.cassandra;

import com.acx.mapping.model.subscriptions.MappingConfig;
import com.acx.mapping.repository.TransformationRepository;
import com.datastax.driver.core.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import play.Logger;
import com.fasterxml.jackson.core.type.TypeReference;

import javax.inject.Inject;
import javax.inject.Singleton;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import static java.lang.String.format;

/**
 *
 */
@Singleton
public class CassandraTransformationRepository extends AbstractRepository implements TransformationRepository
{
    private static final String MAPPING_ID = "mappingId";

    private final Logger.ALogger logger = Logger.of(this.getClass());

    private final Session session;
    private PreparedStatement insertStatement;
    private PreparedStatement deleteStatement;
    private PreparedStatement selectAllStatement;
    private PreparedStatement selectOneStatement;

    @Inject
    public CassandraTransformationRepository(CassandraCluster cluster, ObjectMapper mapper)
    {
        super(cluster, mapper);
        this.session = cluster.getSession();
        selectOneStatement = getSelectOneStatement();
        selectAllStatement = getSelectAllStatement();
        insertStatement = getInsertStatement();
        deleteStatement = getDeleteStatement();
    }

    @Override
    public MappingConfig create(final MappingConfig transformationCfg)
    {
        String id = UUID.randomUUID().toString();
        BoundStatement statement =
            insertStatement.bind().setString(0, id).setString(1, transformationCfg.getName()).setString(2,
                transformationCfg.getType()).setString(3, toJson(transformationCfg.getConfig()));
        execute(statement);
        return read(id).get();
    }

    @Override
    public void update(final String id, final MappingConfig transformationCfg)
    {
        BoundStatement statement = insertStatement.bind().setString(1, transformationCfg.getName()).setString(2,
            transformationCfg.getType()).setString(3, toJson(transformationCfg.getConfig()));
        execute(statement);
    }

    @Override
    public Optional<MappingConfig> read(String id)
    {
        BoundStatement statement = selectOneStatement.bind().setString(MAPPING_ID, id);
        Row row = execute(statement).one();
        return Optional.ofNullable(row).map(this::toMappingConfig);
    }

    @Override
    public List<MappingConfig> readAll()
    {
        BoundStatement statement = selectAllStatement.bind();
        ResultSet resultSet = execute(statement);
        return resultSet.all().stream().map(this::toMappingConfig).collect(Collectors.toList());
    }

    private MappingConfig toMappingConfig(final Row row)
    {
        String id = row.getString(0);
        String name = row.getString(1);
        String type = row.getString(2);
        Map<String, String> config = fromJson(row.getString(3), new TypeReference<HashMap<String, String>>()
        {
        });

        return new MappingConfig(id, name, type, config);
    }

    @Override
    public void delete(final String id)
    {
        BoundStatement statement = deleteStatement.bind().setString(MAPPING_ID, id);
        execute(statement);
    }

    private PreparedStatement getInsertStatement()
    {
        return session.prepare("INSERT INTO " + getTableName() + " (id, name, type, config) VALUES (?, ?, ?, ?);");
    }

    private PreparedStatement getSelectAllStatement()
    {
        return cluster.getSession().prepare(format("SELECT id, name, type, config FROM %s;", getTableName()));
    }

    private PreparedStatement getSelectOneStatement()
    {
        return cluster.getSession().prepare(
            format("SELECT id, name, type, config FROM %s WHERE id=:%s;", getTableName(), MAPPING_ID));
    }

    private PreparedStatement getDeleteStatement()
    {
        return cluster.getSession().prepare(format("DELETE FROM %s WHERE id = :%s;", getTableName(), MAPPING_ID));
    }

    private String getTableName()
    {
        return cluster.getTable("type_configs");
    }
}
