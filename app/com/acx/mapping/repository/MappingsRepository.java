package com.acx.mapping.repository;

import com.acx.mapping.model.subscriptions.MappingConfig;

import java.util.List;
import java.util.Optional;

public interface MappingsRepository
{
    MappingConfig create(MappingConfig subscription);

    void update(String id, MappingConfig subscription);

    Optional<MappingConfig> read(String id);

    List<MappingConfig> readAll();

    void delete(String id);
}
