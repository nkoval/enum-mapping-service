openapi: 3.0.1
info:
  title: Mapping Service
  description: RESTful Service for enum mappings
  version: 1.0.0
  license:
    name: Asset Control
    url: http://www.asset-control.com
tags:
  - name: tasks
    description: Start or get options for tasks

schemes:
- "https"
- "http"

paths:

  /:
    get:
      summary: "index"
      responses:
        200:
          description: "Successful"
          content:
            "text/html":
              schema:
                type: "string"

  /tasks/run:
    post:
      tags:
      - tasks
      summary: "Run a task"
      operationId: "run_task"
      requestBody:
        content:
          'application/json':
            schema:
              $ref: '#/definitions/task'
        required: true
      responses:
        200:
          description: "Task acknowledged and saved"
          content:
            application/json:
              schema:
                type: number

  /tasks/options:
    get:
      tags:
      - tasks
      summary: "Get all available options for a task"
      operationId: "get_available_task_options"
      responses:
        200:
          description: "Successful"
          content:
            'application/json':
               schema:
                 $ref: '#/definitions/taskOptions'

  /tasks/current:
    get:
      tags:
        - tasks
      summary: "Get all currently running tasks"
      operationId: "get_currently_running_tasks"
      responses:
        200:
          description: "Successful"
          content:
            'application/json':
              schema:
                type: "string"

  /tasks:
    get:
      tags:
        - tasks
      summary: "Get all saved tasks"
      operationId: "get_tasks"
      parameters:
        - $ref: '#/parameters/taskName'
        - $ref: '#/parameters/id'
      responses:
        200:
          description: "Successful"
          content:
            'application/json':
              schema:
                $ref: '#/definitions/taskModel'

definitions:
  task:
    type: "object"
    properties:
      id:
        type: "string"
        nullable: false
      taskName:
        type: "string"
        nullable: false
      taskData:
        $ref: '#/definitions/taskData'
      lastRunTime:
        type: "integer"
        format: "int64"
        nullable: true

  taskOptions:
    type: array
    items:
      type: object
      properties:
        datasetIds:
          type: array
          items:
            type: object
            properties:
              dataSetId:
                type: string
          nullable: false
        adapters:
          type: array
          items:
            type: object
            properties:
              adapterConfig:
                type: string
          nullable: false

  taskModel:
    type: "array"
    items:
      type: "object"
      properties:
        id:
          type: "string"
          nullable: false
        taskName:
          type: "string"
          nullable: false
        taskData:
          $ref: '#/definitions/taskData'
        lastRunTime:
          type: "integer"
          format: "int64"
          nullable: true
        started:
          type: "integer"
          format: "int64"
          nullable: true
        finished:
          type: "integer"
          format: "int64"
          nullable: true
        state:
          type: "string"
          nullable: true
                          
  taskData:
    type: "object"
    properties:
      datasetId:
        type: "string"
        nullable: false
      adapterType:
        type: "string"
        nullable: false
      adapterConfig:
        type: "string"
        nullable: false

parameters:

  id:
    name: "id"
    description: "Unique id of the task"
    in: "query"
    required: false
    schema:
      type: "string"

  taskName:
    name: "taskName"
    description: "Name of the task"
    in: "query"
    required: false
    schema:
      type: "string"


