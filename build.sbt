import com.typesafe.sbt.packager.docker._
import sbt.Keys.libraryDependencies
resolvers += "AccessControl" at "http://nexus-repository2.asset-control.com/repository/releases"
resolvers += "AccessControlSnap" at "http://nexus-repository2.asset-control.com/repository/snapshots"
resolvers += Resolver.DefaultMavenRepository
resolvers += Resolver.jcenterRepo
lazy val baseSettings = Seq(
  name := "Mapping Service",
  organization := "asset-control",
  organizationName := "Asset Control",
  organizationHomepage := Some(url("http://asset-control.com")),
  maintainer   := "acplusback-endteam@asset-control.com",
  scalaVersion := "2.12.8",
  scalacOptions ++= Seq(
    "-feature",                 // Shows warnings in detail in the stdout
    "-language:reflectiveCalls" // Address the warning generated on the primary route files after introducing subprojects; see https://groups.google.com/d/msg/play-framework/nNr2NdBtWuw/JfbPLaX35XcJ
  ),
  // Exclude API documentation
  sources in (Compile, doc) := Seq.empty,
  publishArtifact in (Compile, packageDoc) := false,
  publishArtifact in Test := false,
  // disable using the Scala version in output paths and artifacts
  crossPaths := false,
  publishMavenStyle := true,
  releaseIgnoreUntrackedFiles := true,
  publishConfiguration := publishConfiguration.value.withOverwrite(true),
  publishLocalConfiguration := publishLocalConfiguration.value.withOverwrite(true),
  credentials += Credentials(Path.userHome / ".sbt" / ".credentials"),
  //UGLY HACK. WORKAROUND FOR https://github.com/sbt/sbt/issues/3570
  updateOptions := updateOptions.value.withGigahorse(false),
  {
    val nexus = "http://ac-m2repo-prod/"
    publishTo := {
      if (version.value.trim.endsWith("SNAPSHOT")) {
        Some("snapshots" at nexus + "repository/snapshots/")
      }
      else
        Some("releases" at nexus + "repository/releases/")
    }
  },
 javacOptions ++= Seq(
    "-deprecation" // Shows a description of each use or override of a deprecated member or class
  ),
 javaOptions in Test += "-Dconfig.file=conf/test/application.conf",
  testOptions += Tests.Argument(TestFrameworks.JUnit, "-v", "-q", "-a"),
  swaggerDomainNameSpaces := Seq("models"),
)
val dockerAliasPrint = taskKey[Unit]("Print out docker name and tag for Jenkins")
lazy val dockerSettings = Seq(
  version in Docker := {
    if ((version in ThisBuild).value.trim.endsWith("SNAPSHOT")) {
       "%s-%s".format((version in ThisBuild).value, git.gitHeadCommit.value.getOrElse(""))
    } else {
      s"${(version in ThisBuild).value}"
    }
  },
  defaultLinuxInstallLocation in Docker := "/opt/ac",
  dockerRepository := Some("ac-m2repo-prod:5000"),
  dockerUpdateLatest := false,
  dockerExposedPorts := Seq(9000), // We actually override this setting below, but this is the only way to prevent a warning that no port is exposed
  dockerCommands := Seq(
    Cmd("FROM", "openjdk:8-jre-alpine"),
    Cmd("LABEL", s"""MAINTAINER="${maintainer.value}""""),
    Cmd("WORKDIR", "/opt/ac"),
    Cmd("COPY", "opt", "/opt"),
    Cmd("RUN", "apk", "add", "--no-cache", "bash"),
    Cmd("RUN", "addgroup", "-g", "9000", "acgroup"),
    Cmd("RUN", "adduser", "-D", "-u", "9000", "acuser", "-G", "acgroup"),
    Cmd("RUN", "chown", "-R", "acuser", "/opt/ac"),
    Cmd("USER","acuser"),
    ExecCmd("ENTRYPOINT", "/opt/ac/start", s"bin/${executableScriptName.value}"),
  ),
)
// // Models module
// lazy val modelsModule = (project in file("modules/acx-batch-publisher-models"))
// // Adapter API module
// lazy val adapterApi = (project in file("modules/acx-batch-publisher-adapter-api"))
lazy val root = (project in file("."))
  .enablePlugins(PlayMinimalJava, DockerPlugin, SwaggerPlugin, UniversalDeployPlugin)
  // .dependsOn(adapterApi, modelsModule)
  // .aggregate(adapterApi, modelsModule)
  .settings(baseSettings, dockerSettings,
    Seq(
      dockerAliasPrint := {
        val alias = (dockerAlias in Docker).value
        val outFile = file("target/DOCKER_TAG")
        alias.registryHost.foreach(reg => IO.write(outFile, reg + "\n"))
        IO.append(outFile, alias.name + "\n")
        alias.tag.foreach(IO.append(outFile, _))
        IO.append(outFile, "\n" + alias + "\n")
      },
      Keys.fork in Test := false,
      updateOptions := updateOptions.value.withLatestSnapshots(false),
      //mappings in Universal ++=
      //  (baseDirectory.value / "dist/db/" * "*" get) map
      //   (x => x -> ("extern/" + x.getName)),
      Universal / mappings := (Universal / mappings).value.filter {
        case (_, path) => !path.contains("conf/test")
      },
      // Extend task packageZipTarball to add classifier 'pkg' and change extension from 'tgz' to 'tar.gz'
      // This naming scheme is more commonly used by AC tools.
      packageZipTarball in Universal := {
        val originalFile = (packageZipTarball in Universal).value
        val (base, ext) = originalFile.baseAndExt
        val newFile = file(originalFile.getParent) / (base + "-pkg.tar.gz")
        IO.move(originalFile, newFile)
        newFile
      },
      //--make sure the zip gets made before the publishing commands for the added artifacts
      publish := (publish dependsOn (packageZipTarball in Universal)).value,
      publishM2 := (publishM2 dependsOn (packageZipTarball in Universal)).value,
      publishLocal := (publishLocal dependsOn (packageZipTarball in Universal)).value,
      PlayKeys.devSettings ++= Seq(
        "play.server.max-header-size" -> "64k",
        "cassandra.contactPoints.0" -> "ops360server-qa",
        "play.filters.hosts.allowed.0" -> ".",
        "play.filters.filters.csrf.header.bypassHeaders.Csrf-Token" -> "nocheck",
        "play.ws.timeout.request" -> "20minutes",
        "services.bdms.url" -> "http://ops360server-qa:6970",
        "play.server.http.port" -> "9017"
      ),
      libraryDependencies ++= Seq(
        guice,
        "com.datastax.cassandra" % "cassandra-driver-core" % "3.7.2",
        "com.datastax.cassandra" % "cassandra-driver-extras" % "3.7.2",
        "junit" % "junit" % "4.12" % Test,
        "org.mockito" % "mockito-all" % "1.10.19" % Test,
        "javax.validation" % "validation-api" % "2.0.1.Final",
        "org.hibernate.validator" % "hibernate-validator" % "6.0.16.Final",
        "org.glassfish" % "javax.el" % "3.0.0",
        "org.webjars" % "swagger-ui" % "3.22.1",
        "com.fasterxml.jackson.core" % "jackson-core" % "2.10.3",
        "com.fasterxml.jackson.dataformat" % "jackson-dataformat-csv" % "2.10.3",
        "asset-control" % "authentication-play" % "1.0.0",
        "asset-control" % "http-exceptions-play" % "1.0.0",
        "com.fasterxml.jackson.core" % "jackson-core" % "2.11.0",
        "com.enragedginger" %% "akka-quartz-scheduler" % "1.8.4-akka-2.6.x",
        javaWs,
        "org.apache.commons" % "commons-lang3" % "3.11",
        "com.google.code.gson" % "gson" % "2.8.6"
      ),
    )
  )
// Release
import sbtrelease.ReleasePlugin.autoImport.ReleaseTransformations._
releaseProcess := Seq[ReleaseStep](
  checkSnapshotDependencies,              // : default ReleaseStep
  inquireVersions,                        // : default ReleaseStep
  runClean,                               // : default ReleaseStep
  runTest,                                // : default ReleaseStep
  setReleaseVersion,                      // : default ReleaseStep
  commitReleaseVersion,                   // : default ReleaseStep, performs the initial git checks
  tagRelease,                             // : default ReleaseStep
  publishArtifacts,                       // : default ReleaseStep, checks whether `publishTo` is properly set up
  releaseStepTask(publish in Docker),     // : additional ReleaseStep, publish docker image
  setNextVersion,                         // : default ReleaseStep
  commitNextVersion,                      // : default ReleaseStep
  pushChanges                             // : default ReleaseStep, also checks that an upstream branch is properly configured
)
