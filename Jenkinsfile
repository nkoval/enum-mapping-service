properties(
    [
        parameters ([
            choice(name: 'DEPLOYMENT_HOST', choices: ['acdba@ops360server-qa', 'acdba@ops360server'], description: 'Server for deployment', defaultValue: 'acdba@ops360server-qa')
            ,string(name: 'RELEASE_VERSION', description: 'The release version for example (1.0.18-rc-1)', defaultValue: '', required: false)
            ,string(name: 'NEXT_VERSION', description: 'Development version for example (1.0.18-SNAPSHOT)', defaultValue: '', required: false)
        ])
    ]
)

node("docker") {
    stage ('build') {
        jdk = tool name: 'openjdk8'
        env.JAVA_HOME = "${jdk}"

        bitbucketStatusNotify(buildKey: 'build', buildName: 'Build', buildState: 'INPROGRESS')
        checkout scm
        try {
            sh "${jdk}/bin/java -jar $HOME/tools/sbt/sbt-launch.jar -Dsbt.log.noformat=true -Dsbt.override.build.repos=true -Dsbt.repository.config=${workspace}/repositories clean compile"
            bitbucketStatusNotify(buildKey: 'build', buildName: 'Build', buildState: 'SUCCESSFUL')
        } catch (err) {
            bitbucketStatusNotify(buildKey: 'build', buildName: 'Build', buildState: 'FAILED')
            error err
        }
    }

    stage ('test') {
        try {
            bitbucketStatusNotify(buildKey: 'test', buildName: 'Tests', buildState: 'INPROGRESS')
            sh "${jdk}/bin/java -jar $HOME/tools/sbt/sbt-launch.jar -Dsbt.log.noformat=true -Dsbt.override.build.repos=true -Dsbt.repository.config=${workspace}/repositories test"
            bitbucketStatusNotify(buildKey: 'test', buildName: 'Tests', buildState: 'SUCCESSFUL')
        } catch (err) {
            bitbucketStatusNotify(buildKey: 'test', buildName: 'Tests', buildState: 'FAILED')
            error err
        }
    }

    if (env.BRANCH_NAME == "develop" || env.BRANCH_NAME.startsWith("release/"))
    {
        stage ('package')
        {
            try {
                bitbucketStatusNotify(buildKey: 'package', buildName: 'Package artifacts', buildState: 'INPROGRESS')
                sh "${jdk}/bin/java -jar $HOME/tools/sbt/sbt-launch.jar -Dsbt.log.noformat=true -Dsbt.override.build.repos=true -Dsbt.repository.config=${workspace}/repositories packageZipTarball"
                bitbucketStatusNotify(buildKey: 'package', buildName: 'Package artifacts', buildState: 'SUCCESSFUL')
            } catch (err) {
                bitbucketStatusNotify(buildKey: 'package', buildName: 'Package artifacts', buildState: 'FAILED')
                error err
            }
        }

        stage ('publish')
        {
            try {
                bitbucketStatusNotify(buildKey: 'publish', buildName: 'Publish to Nexus', buildState: 'INPROGRESS')
                sh "${jdk}/bin/java -jar $HOME/tools/sbt/sbt-launch.jar -Dsbt.log.noformat=true -Dsbt.override.build.repos=true -Dsbt.repository.config=${workspace}/repositories aetherDeploy docker:publish dockerAliasPrint"
                bitbucketStatusNotify(buildKey: 'publish', buildName: 'Publish to Nexus', buildState: 'SUCCESSFUL')
            } catch (err) {
                bitbucketStatusNotify(buildKey: 'publish', buildName: 'Publish to Nexus', buildState: 'FAILED')
                error err
            }
        }

        archiveArtifacts artifacts: 'target/DOCKER_TAG', onlyIfSuccessful: true, fingerprint: true

        stage('deploy')
        {
            try
            {
                bitbucketStatusNotify(buildKey: 'deploy', buildName: 'Deploy to CD system', buildState: 'INPROGRESS')
                sh "${jdk}/bin/java -jar $HOME/tools/sbt/sbt-launch.jar -Dsbt.log.noformat=true -Dsbt.override.build.repos=true -Dsbt.repository.config=${workspace}/conf/repositories dockerAliasPrint"

                COMPOSE_SERVICE_NAME = "mapping-service"

                REPO_NAME = sh(
                        script: 'awk \'NR==1\' target/DOCKER_TAG',
                        returnStdout: true
                ).trim()
                IMAGE_NAME = sh(
                        script: 'awk \'NR==2\' target/DOCKER_TAG',
                        returnStdout: true
                ).trim()
                DOCKER_TAG = sh(
                        script: 'awk \'NR==3\' target/DOCKER_TAG',
                        returnStdout: true
                ).trim()

                sh "ssh -o StrictHostKeyChecking=no -t ${params.DEPLOYMENT_HOST} \"cd docker && ./replaceTag.sh ${REPO_NAME}\\\\\\\\/${IMAGE_NAME} ${DOCKER_TAG}\""
                sh "ssh -o StrictHostKeyChecking=no -t ${params.DEPLOYMENT_HOST} \"cd docker && docker-compose pull && docker-compose up -d --no-deps --build ${COMPOSE_SERVICE_NAME}\""

                bitbucketStatusNotify(buildKey: 'deploy', buildName: 'Deploy to CD system', buildState: 'SUCCESSFUL')
            }
            catch (err)
            {
                    bitbucketStatusNotify(buildKey: 'deploy', buildName: 'Deploy to CD system', buildState: 'FAILED')
                    error err.message
            }
        }
    }

    if (env.BRANCH_NAME.startsWith("release/"))
    {

        stage('release')
        {
        	when {
                expression { params.NEXT_VERSION }
            }
            steps
            {
    
        
	            script
	            {
	            	echo 'Check versions are set'
	//                 def proceed = true
	//                 try {
	//                     timeout(time: 300, unit: "SECONDS") {
	//                         input {
	//                             message "Perform release?"
	//                             parameters {
	//                                 string(name: 'RELEASE_VERSION', description: 'The release version for example (1.0.18-rc-1)')
	//                                 string(name: 'NEXT_VERSION', description: 'Development version for example (1.0.18-SNAPSHOT)')
	//                             }
	//                         }
	//                     }
	//                 } catch (err) {
	//                     proceed = false
	//                 }
	                echo 'Check versions are set'
//	                if ( params.RELEASE_VERSION.isEmpty() ||  params.NEXT_VERSION.isEmpty() )
//	                {
	                	echo 'Checking params'
	//	                if ("${params.RELEASE_VERSION}" =~ /\d+\.\d+.*/ && "${params.NEXT_VERSION}" =~ /\d+\.\d+.*/)
	//	                {
	//	                    echo 'Performing release'
	//	                    try {
	//	                        bitbucketStatusNotify(buildKey: 'release', buildName: 'Perform release', buildState: 'INPROGRESS')
	//	                        sh "${jdk}/bin/java -jar $HOME/tools/sbt/sbt-launch.jar -Dsbt.log.noformat=true -Dsbt.override.build.repos=true -Dsbt.repository.config=${workspace}/repositories release release-version $RELEASE_VERSION next-version $NEXT_VERSION default-tag-exists-answer k"
	//	                        sh 'git push && git push --tags'
	//	                        bitbucketStatusNotify(buildKey: 'release', buildName: 'Perform release', buildState: 'SUCCESSFUL')
	//	                    } catch (err) {
	//	                        bitbucketStatusNotify(buildKey: 'release', buildName: 'Perform release', buildState: 'FAILED')
	//	                        error err
	//	                    }
	//	                }
	//	                else
	//	                {
	//	                    echo 'WARNING: Both RELEASE_VERSION and NEXT_VERSION parameters have to be set to perform a release'    
	//					}
//					}
//					else
//					{    
//		                echo 'Both RELEASE_VERSION and NEXT_VERSION parameters have to be set to perform a release'
//					}
	            }
			}
        }
    }


    stage('finish') {
        echo 'Finished...'
    }
}

